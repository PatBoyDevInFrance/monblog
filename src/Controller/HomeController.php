<?php

namespace App\Controller;

use App\Repository\ArticleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    #[Route('/', name: 'home')]
    public function index(ArticleRepository $repo): Response
    {   
        $articles = $repo->findAll();
        //dd($articles);

        return $this->render('home/index.html.twig',[
            'articles' => $articles
        ]);
    }

    #[Route('/show/{id}', name: 'show')]
    public function show($id , ArticleRepository $repo): Response
    {   
        $articles = $repo->find($id);
        //dd($articles);

        return $this->render('show/index.html.twig',[
            'articles' => $articles
        ]);
    }
}
